<?php

namespace App\Http\Controllers;


use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\Routing\ResponseFactory;
use Session;
use Input;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Models\Usuario;  //<--!!-->importar modelo de controlador
use App\Http\Models\MateriaPrima;  //<--!!-->importar modelo de controlador
use Redirect;

use Illuminate\Support\Facades\Crypt;//desencripta


//----Controlador de materia prima
class C_Materia_prima extends Controller
{


    protected function getRegistrarMateria() {
      return view('materias_primas/registro_materia_prima');
        //return redirect('login');
    }

    //---capturar datos de vista login
    protected function postRegistrarMateria(Request $request) {

      //validar
        $materiaPrima = new MateriaPrima;
        // modificar
        //$materiaPrima->persona_id = 2;
        $materiaPrima->persona_id = Session::get('usuario.persona_id');
        //$materiaPrima->materia_prima_id = 1;---->>AUTOINCREMENTS
        $materiaPrima->nombre_materia_prima = $request['nombre_materia_prima'];
        $materiaPrima->tipo = $request['tipo'];
        $materiaPrima->descripcion = $request['descripcion'];
        $materiaPrima->unidad_de_medida = $request['unidad_de_medida'];


        if ($materiaPrima->save()) {

            return redirect('materia_prima')->with('success', 'Materia prima registrada correctamente');
        }
    }



    protected function getListarMateriaPrima() {
      if (Session::get('usuario')) {
          //$materiasPrimas = MateriaPrima::all();

          //--colsultar materias primas de user session
          $materiasPrimas = MateriaPrima::where([
                     'persona_id' => Session::get('usuario.persona_id'),
                 ])->get();
        // $materiasPrimas = MateriaPrima::paginate(10);
        // $materiasPrimas->setPath('listarMateriaPrima');

        return view('materias_primas/listar_materias', compact('materiasPrimas'));

      }else{
          //echo("<script>alert('NO hay DATOS de session');</script>");
          //-->> mensaje de error----
          return redirect('login');
      }

  }





protected function getEditarMateriaPrima(Request $request) {


  if (Session::get('usuario')) {

    //--materia_prima_id de MATERIA para editar
    $materia_prima_id = $request['materia_prima_id'];

    //--colsultar materias primas de user session
    //$materiaPrimaSeleccionada = MateriaPrima::where('materia_prima_id', $materia_prima_id)->get();
    $materiaPrimaSeleccionada = MateriaPrima::where([
               //'persona_id' => Session::get('usuario.persona_id'),
               'materia_prima_id' =>  $materia_prima_id,
           ])->get();


    $mi_materiaPrima = $materiaPrimaSeleccionada[0];

    //--enviar materia seleccionada para editar
    return view('materias_primas/editar_materia_prima', compact('mi_materiaPrima'));

  }else{
      //echo("<script>alert('NO hay DATOS de session');</script>");
      //-->> mensaje de error----
      return redirect('login');
  }

}



protected function postEditarMateriaPrima(Request $request) {


  //--crear Usuario para usar sus funciones
  $mi_Usuario = new Usuario;

  //==>> VERIFICAR SESSION ACTIVA (si esta logueado)
  if ($mi_Usuario->isLogin()) {
    //echo ("SI login");

    //--obtener id
    $materia_prima_id = $request['materia_prima_id'];

    //--update
    if ($materiaPrimaActualizada = MateriaPrima::where('materia_prima_id', $materia_prima_id)
                    ->update(['nombre_materia_prima'  => $request['nombre_materia_prima'],
                              'tipo'                  => $request['tipo'],
                              'descripcion'           => $request['descripcion'],
                              'unidad_de_medida'      => $request['unidad_de_medida'],
                    ])) {

        return redirect('materia_prima')->with('success', 'materia prima editada correctamente');
        //return redirect('materia_prima')->with('error', 'error al editar');
    } else {
        return redirect('materia_prima')->with('error', 'error al editar');
    }
  }else{
    //echo ("NO login");
    return redirect('login');
  }



 }


 protected function getEliminarMateriaPrima(Request $request) {

   //echo "<script> console.log('consola'); </script>";


   //--crear Usuario para usar sus funciones
   $mi_Usuario = new Usuario;

   if ($mi_Usuario->isLogin()) {

     $mi_Usuario->nombre_usuario  = "loco";
     $mi_Usuario->persona_id  = 2;

   }





   $mi_Usuario->mi_query();

   $mi_Usuario->llenar_arrays();


   print_r($mi_Usuario->array_materias_primas);

   //return redirect($mi_Usuario->var1);


 }





  protected function getEliminarMateriaPrima2(Request $request) {


    //--crear Usuario para usar sus funciones
    $mi_Usuario = new Usuario;

    //==>> VERIFICAR SESSION ACTIVA (si esta logueado)
    if ($mi_Usuario->isLogin()) {
      //echo ("SI login");

      //--obtener id
      $materia_prima_id = $request['materia_prima_id'];

      $materiaPrima = new MateriaPrima;

      //--1)search
      $materiaPrima = MateriaPrima::find($materia_prima_id);
      //--2)delete
       if ($materiaPrima->delete()) {
           return redirect('materia_prima')->with('success', 'materia prima eliminada correctamente');
        }else {
          return redirect('materia_prima')->with('error', 'error al eliminar');
        }
    }else{
      //echo ("NO login");
      return redirect('login');
    }

  }








}
