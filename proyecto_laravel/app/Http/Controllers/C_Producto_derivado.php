<?php

namespace App\Http\Controllers;


use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\Routing\ResponseFactory;
use Session;
use Input;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Models\Usuario;  //<--!!-->importar modelo de controlador
use App\Http\Models\ProductoLacteo;  //<--!!-->importar modelo de controlador
use App\Http\Models\MateriaPrima;  //<--!!-->importar modelo de controlador
use App\Http\Models\Ingrediente;  //<--!!-->importar modelo de controlador
use Redirect;

use Illuminate\Support\Facades\Crypt;//desencripta


//----Controlador de materia prima
class C_Producto_derivado extends Controller
{

    protected function getRegistrarProducto() {
      return view('productos/registro_producto');
        //return redirect('login');
    }

    //---capturar datos de vista login
    protected function postRegistrarProducto(Request $request) {

        //validar


        //--------- insertar ProductoLacteo ------------
        $productoLacteo = new ProductoLacteo;

        //--agregar variables a objeto Ingrediente
        $productoLacteo->persona_id = Session::get('usuario.persona_id');
        //$productoLacteo->producto_id = 1;---->>AUTOINCREMENTS
        $productoLacteo->nombre_producto_lacteo=$request['nombre_producto_lacteo'];
        $productoLacteo->tipo = $request['tipo'];
        $productoLacteo->descripcion = $request['descripcion'];
        $productoLacteo->dias_tiempo_de_vencimiento =$request['dias_tiempo_de_vencimiento'];
        $productoLacteo->instrucciones = $request['instrucciones'];
        $productoLacteo->unidad_de_medida = $request['unidad_de_medida'];


        //--INSERTAR ProductoLacteo
        if ($productoLacteo->save()) {


          //--------- insertar Ingredientes ------------
          $mi_obj_ingrediente = new Ingrediente;

          //--obtener variables
          $mi_producto_id = $productoLacteo->producto_id ;
          $mi_materia_id = 1 ;
          $mi_orden = 1 ;

          //--agregar variables a objeto Ingrediente
          $mi_obj_ingrediente->producto_id = $mi_producto_id;
          $mi_obj_ingrediente->materia_prima_id = $mi_materia_id;//--desde Request
          $mi_obj_ingrediente->orden = $mi_orden; //--desde Request

          //--INSERTAR Ingrediente
          if ($mi_obj_ingrediente->save()) {
            return redirect('producto_derivado')->with('success', 'producto e ingredientes registrados correctamente');
          }

          return redirect('xxx-ingrediente-xxx');

        }
    }



    protected function getRegistrarProducto2(Request $request) {

      //$array = json_decode($request['data'], true);
       //echo "<script> console.log('pepe'); </script>";

       //echo("hola");

       return "return";


    }






    protected function getListarProductoDerivado(Request $request) {

      //--verificar si esta logueado
      if (!Session::get('usuario')) {
        return redirect('login');//---no login
      }else {
        //---si login

        //--funcion de admin
        if ($request['persona_id']) {
          //--colsultar productos de user con persona_id
          $productosLacteos = ProductoLacteo::where([
                     'persona_id' => $request['persona_id'],
                 ])->get();
        }else {

          //--colsultar productos de user session
          $productosLacteos = ProductoLacteo::where([
                     'persona_id' => Session::get('usuario.persona_id'),
                 ])->get();
        }



      return view('productos/listar_productos', compact('productosLacteos'));

      }


  }



  protected function getEditarProducto(Request $request) {

    //--verificar si esta logueado
    if (!Session::get('usuario')) {
      return redirect('login');//---no login
    }else {
      //---si login

      //--producto_id de PRODUCTO para editar
      $producto_id = $request['producto_id'];

      $productoSeleccionado = ProductoLacteo::where([
                 //'persona_id' => Session::get('usuario.persona_id'),
                 'producto_id' =>  $producto_id,
             ])->get();


      $mi_producto = $productoSeleccionado[0];

      //--enviar materia seleccionada para editar
      return view('productos/editar_producto', compact('mi_producto'));

    }

  }



  //-- UPDATE
  protected function postEditarProducto(Request $request) {

    //--verificar si esta logueado
    if (!Session::get('usuario')) {
      return redirect('login');//---no login
    }else {
      //---si login

      //--producto_id de PRODUCTO para editar
      $producto_id = $request['producto_id'];


      //--update
      if ($productoActualizado = ProductoLacteo::where('producto_id', $producto_id)
                      ->update(['nombre_producto_lacteo'  => $request['nombre_producto_lacteo'],
                                'tipo'                    => $request['tipo'],
                                'descripcion'             => $request['descripcion'],
                                'dias_tiempo_de_vencimiento'      => $request['dias_tiempo_de_vencimiento'],
                                'instrucciones'           => $request['instrucciones'],
                                'unidad_de_medida'        => $request['unidad_de_medida'],
                      ])) {

          return redirect('producto_derivado')->with('success', 'producto editado correctamente');
      } else {
          return redirect('producto_derivado')->with('error', 'error al editar');
      }

    }



   }




   //-->> DELETE
   protected function getEliminarProducto(Request $request) {

     //--verificar si esta logueado
     if (!Session::get('usuario')) {
       return redirect('login');//---no login
     }else {
       //---si login

       //--producto_id de PRODUCTO para eliminar
       $producto_id = $request['producto_id'];

       $mi_producto = new ProductoLacteo;

       //--1)search
       $mi_producto = ProductoLacteo::find($producto_id);

       //--2)delete
        if ($mi_producto->delete()) {
            return redirect('producto_derivado')->with('success', 'producto eliminado correctamente');
         }else {
           return redirect('producto_derivado')->with('error', 'error al eliminar');
         }

     }


   }









}
