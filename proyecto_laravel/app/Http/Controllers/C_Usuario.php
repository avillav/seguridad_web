<?php

namespace App\Http\Controllers;


use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\Routing\ResponseFactory;
use Session;
use Input;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Models\Usuario;  //<--!!-->importar modelo de controlador
use App\Http\Models\Persona;  //<--!!-->importar modelo de controlador
use Redirect;

use Illuminate\Support\Facades\Crypt;//desencripta


//----Controlador de Usuario
class C_Usuario extends Controller
{


use AuthenticatesAndRegistersUsers,
    ThrottlesLogins;


//$mi_Usuario = new Usuario;


    public function __construct(Guard $auth) {
        $this->auth = $auth;
        //$this->middleware('guest', ['except' => 'getLogout']);
    }


    //============= Login =============

    //---mostrar vista login
    protected function getLogin() {
        return view("V_login");
        //return redirect('login');
    }


    //---mostrar vista login--despues de logout
    protected function getLogout() {

        $this->auth->logout();
        Session::flush();  //---->>eliminar session
        //return view("V_login");
        return redirect('login');

    }



    //---capturar datos de vista login
    protected function postLoginUsuario(Request $request) {

        //-->capturar datos
        $nombre_usuario = $request['nombre_usuario'];
        $password = $request['password'];


        //====>> Query --> 2 condicionales anidados
        $usuario_Seleccionado = Usuario::where([
                        'nombre_usuario' => $nombre_usuario,
                        //'password' => $password,
                    ])->get();


        //--verificar si los datos son correctos
        if (count($usuario_Seleccionado)) {

             //echo('SI existe usuario');echo('<br>');
            //--capturar OBJETO usuario
            $mi_usuario = $usuario_Seleccionado[0];//--exception si no hay datos

            //--DES-ENCRIPTAR PASSWORD de BD
            $password_desencriptado = Crypt::decrypt($mi_usuario['password']);

            //--verificar si password DESENCRIPATO es correcto
            if (strcmp($password, $password_desencriptado) == 0) { //-->> =0 son iguales

                 //--variables de usuario
                 $usuario_session['persona_id'] = $mi_usuario['persona_id'];
                 $usuario_session['nombre_usuario'] = $mi_usuario['nombre_usuario'];
                 $usuario_session['password'] = $mi_usuario['password'];
                 $usuario_session['rol'] = $mi_usuario['rol'];

                 //--colsultar atributos persona ---de padre- SuperTipo
                 $persona_usuario_Seleccionado = Persona::where([
                            'persona_id' => $usuario_session['persona_id'],
                        ])->get();

                 //--agregar nombre--atributo de padre
                 $usuario_session['nombre'] = $persona_usuario_Seleccionado['0']['nombre'];
                 //print_r($usuario_session);

                 //--inicializar SESSION
                 Session::put('usuario', $usuario_session);
                 //$rol = Session::get('usuario.rol');


                 //--redireccionar vista
                 //===========>>>preguntar x rol
                 //return redirect('home');
                 return redirect('home')->with('status', 'Profile updated!');
                  //return redirect('home')->with('mi_usuario_login', $usuario_session);
                 //return view('usuarios/editar_usuario', compact('mi_usuario'));


             //--salir si password incorrecto
             }else{
               echo("<script>alert('DATOS incorrectos');</script>");
               return view("V_login");

               //return redirect('/')->with('success', 'usuario editado correctamente');
               //return redirect("/")->with('errors', 'error en login2');
             }

         //--salir si no existe nombre_usuario

        }else{
             echo("<script>alert('DATOS incorrectos');</script>");
             return view("V_login");

             //return redirect('/')->with('success', 'usuario editado correctamente');
             //return redirect("/")->with('errors', 'error en login2');
        }


    }




    //---mostrar vista de Menu
    protected function getHome() {

        //--crear Usuario para usar sus funciones
        $mi_Usuario = new Usuario;

        //==>> VERIFICAR SESSION ACTIVA (si esta logueado)
        if ($mi_Usuario->isLogin()) {
          //echo ("SI login");
          return view("home");
        }


    }



    //---mostrar vista registrar usuario
    protected function getRegistrarUsuario() {
        return view('usuarios/registro_usuario');
    }


    //---capturar datos de vista registrar usuario
    protected function postRegistrarUsuario(Request $request) {

      //validar datos obligatorios

      //-->capturar datos
      $nombre         = $request['nombre'];
      $nombre_usuario = $request['nombre_usuario'];
      $password       = $request['password'];
      $rol            = $request['rol'];

      //--1)crear persona --2=crear usuario
      $persona_nueva = new Persona;
      $usuario_nuevo = new Usuario;

      $persona_nueva->nombre          = $nombre;
      $usuario_nuevo->nombre_usuario  = $nombre_usuario;
      //$usuario_nuevo->password        = $password;
      $usuario_nuevo->password        = Crypt::encrypt($password);//--encriptar password
      $usuario_nuevo->rol             = $rol;

      if ($persona_nueva->save()) {
        $usuario_nuevo->persona_id  = $persona_nueva->persona_id;

        if ($usuario_nuevo->save()) {

            return redirect('login')->with('success', 'Usuario registrado correctamente');
        }
      }





    }



    //---mostrar vista registrar usuario
    protected function getEditarUsuario() {

      //--verificar si esta logueado
      if (!Session::get('usuario')) {
        return redirect('login');//---no login
      }else {
        //---si login

        $mi_usuario = Session::get('usuario');

        return view('usuarios/editar_usuario', compact('mi_usuario'));

      }

    }




    protected function getListarUsuarios() {

      //--verificar si esta logueado
      if (!Session::get('usuario')) {
        return redirect('login');//---no login
      }else {

        //--verificar si es director
        if (Session::get('usuario.rol') == 'Director') {
          $all_usuarios = Usuario::all();
          return view('usuarios/listar_usuarios', compact('all_usuarios'));
        }else {
          return redirect('login');//---no login
        }



      }

  }








}
