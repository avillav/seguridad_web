<?php


namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model {


    protected $table = 'ingrediente'; //--tabla interseccion entre (producto_lacteo X materia_prima)
    //-- clave primaria COMPUESTA
    //$table->primary(['materia_prima_id', 'producto_id']);//CP compuesta
    //protected $primaryKey = ['materia_prima_id', 'producto_id'];//CP compuesta


    protected $fillable = [
        'materia_prima_id',     //--foranea hacia (materia_prima)
        'producto_id',          //--foranea hacia (producto_lacteo)

        'orden'
    ];




}
