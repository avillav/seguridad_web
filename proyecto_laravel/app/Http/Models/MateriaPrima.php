<?php


namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class MateriaPrima extends Model {


    protected $table = 'materia_prima';
    protected $primaryKey = 'materia_prima_id';


    protected $fillable = [
        'materia_prima_id',
        'nombre_materia_prima',
        'descripcion',
        'tipo',
        'unidad_de_medida'
    ];


    //protected $hidden = ['password', 'remember_token'];

}
