<?php


namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model {


    protected $table = 'persona';
    protected $primaryKey = 'persona_id';


    protected $fillable = [
        'persona_id',
        'nombre'
        
    ];


    //protected $hidden = ['password', 'remember_token'];

}
