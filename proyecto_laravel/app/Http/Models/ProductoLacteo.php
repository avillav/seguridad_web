<?php


namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductoLacteo extends Model {

  /*
  $array = array(0 => 100, "color" => "red");
  print_r(array_keys($array));

  Array
(
    [0] => 0
    [1] => color
)
  */


    protected $table = 'producto_lacteo';
    protected $primaryKey = 'producto_id';


    protected $fillable = [

        'persona_id',                   //----->>clave foranea de Usuario
        'producto_id',
        'nombre_producto_lacteo',
        'descripcion',
        'tipo',
        'dias_tiempo_de_vencimiento',  //------>>fecha vencimiento
        'instrucciones',               //--opcional
        'unidad_de_medida'
    ];

    //protected $hidden = ['password', 'remember_token'];
}
