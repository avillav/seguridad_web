<?php


namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Session;
use Log;
//use Illuminate\Database\Eloquent\Builder;

class Usuario extends Persona { //----HERENCIA


  //---ARRAYS de productos y materias_primas
  public $array_productos       = array();
  public $array_materias_primas = array();


  //---- CONSTRUCTOR
  public function __construct(array $attributes = array()){

      //$this->var2 = 777;
      parent::__construct($attributes);
  }

    //---- VARIABLES DE TABLA DE BD (para migracion)
  protected $table = 'usuario';
  protected $primaryKey = 'persona_id';

  protected $fillable = [
  	  'persona_id', //--->> $primaryKey
      'nombre_usuario',
      'password',
      'rol'
  ];




  /*==============================================*/
  /*================ MIS FUNCIONES ===============*/
  /*==============================================*/


  //===========>> llenar vectores <<===========

  //--materias[]----productos[]
  public function llenar_arrays(){

    //--llamar las 2 funciones de llenar array
    $this->llenar_array_materias_primas();
    $this->llenar_array_productos();

  }


  //--materias[]
  public function llenar_array_materias_primas(){

    $this->array_materias_primas = MateriaPrima::where([
               //'persona_id' => Session::get('usuario.persona_id'),
               'persona_id' => $this->persona_id,
           ])->get();

  }


  //--productos[]
  public function llenar_array_productos(){

    $this->array_productos = ProductoLacteo::where([
               //'persona_id' => Session::get('usuario.persona_id'),
               'persona_id' => $this->persona_id,
           ])->get();

  }

  //--fin llenar_arrays
  /*==============================================*/






    public function isLogin()
    {


      //--preguntar si existe alguien logueado
      if (Session::get('usuario')) {
        return true;
      }else{
        //return false;
        return view('xxxx');
        //return redirect('xxxx');
      }

    }












    public function mi_query2(){

    //====>> Query --> 2 condicionales anidados
    $usuario_Seleccionado = Usuario::where([
                    'nombre_usuario' => 'miller',
                    //'password' => $password,
                ])->get();


    print_r($usuario_Seleccionado);

    }


    public function mi_query3(){

      $materiasPrimas = MateriaPrima::where([
                 'persona_id' => Session::get('usuario.persona_id'),
             ])->get();


    print_r($materiasPrimas);

    }



    public function mi_query(){

      print_r($this->array_materias_primas);

      echo "<br>";echo "<br>";

      $this->array_materias_primas = MateriaPrima::where([
                 'persona_id' => Session::get('usuario.persona_id'),
             ])->get();


    print_r($this->array_materias_primas);

    echo "<br>";echo "<br>";

    $this->mi_query_continue();

    }


    public function mi_query_continue(){

      print_r($this->array_materias_primas);


    echo "<br>";echo "<br>";


    echo $this->var2;

    echo "<br>";echo "<br>";

    echo $this->nombre_usuario;

    echo "<br>";echo "<br>";

    }






    //protected $hidden = ['password', 'remember_token'];

}
