<?php

//*******  Controlador de RUTAS *********
//------- RUTAS DE APLICACION -----------


//========== Login y Home ============================================
//--> login
Route::get('/', 'C_Usuario@getLogin');//-->ruta, controlador@funcion
Route::get('login', 'C_Usuario@getLogin');//-->ruta, controlador@funcion


Route::post('action_login_usuario',
		['as' => 'action_login_usuario',
		'uses' => 'C_Usuario@postLoginUsuario']
	);

//--> logout
Route::get('logout', 'C_Usuario@getLogout');//-->ruta, controlador@funcion

//--> home
Route::get('home', 'C_Usuario@getHome');//-->ruta, controlador@funcion






//========== Usuario ============================================
//--registrar

Route::get('registro_usuario', 'C_Usuario@getRegistrarUsuario');//-->ruta, controlador@funcion

Route::post('registro_usuario',
		['as' => 'registro_usuario',
		'uses' => 'C_Usuario@postRegistrarUsuario']
	);

//--editar
Route::get('editar_usuario', 'C_Usuario@getEditarUsuario');//-->ruta, controlador@funcion

//--listar
Route::get('home_administrador', 'C_Usuario@getListarUsuarios');//-->ruta, controlador@funcion


Route::get('usuario', function () {
    return view('home');
});
Route::get('menu', function () {
    return view('usuarios/menu_usuario');
});



//========== Materia prima ============================================
//--registrar
Route::get('registro_materia_prima', 'C_Materia_prima@getRegistrarMateria');

Route::post('registro_materia_prima',
		['as' => 'registro_materia_prima',
		'uses' => 'C_Materia_prima@postRegistrarMateria']
	);

//-- listar
Route::get('materia_prima', 'C_Materia_prima@getListarMateriaPrima');

//-- editar
Route::get('editar_materia_prima', 'C_Materia_prima@getEditarMateriaPrima');

Route::post('editar_materia_prima',
		['as' => 'editar_materia_prima',
		'uses' => 'C_Materia_prima@postEditarMateriaPrima']
	);

//-- eliminar
Route::get('eliminar_materia_prima', 'C_Materia_prima@getEliminarMateriaPrima');



//========== Producto lacteo ============================================
//--registrar
Route::get('registro_producto', 'C_Producto_derivado@getRegistrarProducto');

Route::post('registro_producto',
		['as' => 'registro_producto',
		'uses' => 'C_Producto_derivado@postRegistrarProducto']
	);

Route::get('registro_producto2', 'C_Producto_derivado@getRegistrarProducto2');


//-- listar producto_derivado
Route::get('producto_derivado', 'C_Producto_derivado@getListarProductoDerivado');

//-- editar
Route::get('editar_producto', 'C_Producto_derivado@getEditarProducto');

Route::post('editar_producto',
		['as' => 'editar_producto',
		'uses' => 'C_Producto_derivado@postEditarProducto']
	);

	//-- eliminar
	Route::get('eliminar_producto', 'C_Producto_derivado@getEliminarProducto');
/*

//---crear usuario --- get y post
Route::get('vista_crear_usuario', 'Usuario_Controlador@getVistaCrearUsario');
Route::post('vista_crear_usuario', ['as' => 'vista_crear_usuario', 'uses' => 'Usuario_Controlador@postVistaCrearUsario']);



//---crear usuario --- get y post
Route::get('usuario_vista_login_off', 'Usuario_Controlador@getVistaLoginUsario');
Route::post('vista_crear_usuario', ['as' => 'vista_crear_usuario', 'uses' => 'Usuario_Controlador@postVistaCrearUsario']);

*/
