<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         Schema::create('persona', function (Blueprint $table) {

            //-------VARIABLES/COLUMNAS DE TABLA -------------------------
            //$table->integer('persona_id')->unsigned();//-->CP
            $table->increments('persona_id');//--increments ==>CP x defecto
            $table->string('nombre');

            //..CREA COLUMNAS created_at y updated_at
            $table->timestamps(); ///----NOTA:VARIBLE NECESARIA PARA SAVE()
            //------------------------------------------------------------

            //-------CLAVES/INDICES DE TABLA -----------------------------
            //-- clave primaria
            //$table->primary('persona_id');


        });


        //--insertar persona --test
        DB::table('persona')->insert([
            'persona_id' => 1,
            'nombre' => 'alejendro',
        ]);

        DB::table('persona')->insert([
            'persona_id' => 2,
            'nombre' => 'miller',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('persona');
    }
}
