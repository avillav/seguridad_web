<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\Crypt;//des- y encripta password

class CreateUsuarioTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         Schema::create('usuario', function (Blueprint $table) {

            //-------VARIABLES/COLUMNAS DE TABLA -------------------------
            $table->integer('persona_id')->unsigned();//-->CP --->CF
            //$table->increments('persona_id', 60);

            $table->string('nombre_usuario');//-->CA;
            $table->string('password');
            //$table->string('rol');
            $table->enum('rol', ['Director', 'Tecnico', 'Estudiante']);

            //..CREA COLUMNAS created_at y updated_at
            $table->timestamps(); ///----NOTA:VARIBLE NECESARIA PARA SAVE()
            //token para session
            $table->rememberToken();
            //------------------------------------------------------------

            //-------CLAVES/INDICES DE TABLA -----------------------------
            //-- Clave Primaria
            $table->primary('persona_id');

            //-- Clave Alternativa
            $table->unique('nombre_usuario');

            //-- Clave Foranea -- de PADRE- SUPERTIPO
            $table->foreign('persona_id')
                ->references('persona_id')
                ->on('persona');
                //->onUpdate('cascade');
            //------------------------------------------------------------


        });


         //--insertar usuario -- Test
         DB::table('usuario')->insert([
            'persona_id' => 1,
            'nombre_usuario' => 'avillav@unal.edu.co',
            //'password' => bcrypt('secret'),
            //'password' => '000',
            'password' => Crypt::encrypt('000'), //--insertar passwoird ENCRIPTADO
            'rol' => 'Director',
        ]);


         DB::table('usuario')->insert([
            'persona_id' => 2,
            'nombre_usuario' => 'miller',
            //'password' => bcrypt('secret'),
            //'password' => '000',
            'password' => Crypt::encrypt('123'), //--insertar passwoird ENCRIPTADO
            'rol' => 'Director',
        ]);

        //$password_encriptado = Crypt::encrypt($password);//--190 caracteres
        //$password_desencriptado = Crypt::decrypt($password_encriptado);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('usuario');
    }






}
