<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriaPrimaTable extends Migration
{


    public function up()
    {
        //
        Schema::create('materia_prima', function (Blueprint $table) {

            //-------VARIABLES/COLUMNAS DE TABLA -------------------------
            $table->integer('persona_id')->unsigned(); //--->CF--hacia usuario
            $table->increments('materia_prima_id');//--increments ==>CP x defecto
            //$table->increments('materia_prima_id', 60);
            $table->string('nombre_materia_prima');
            $table->string('descripcion');
            //$table->string('tipo');
            $table->enum('tipo', ['Comestible', 'Empaque']);
            //$table->string('unidad_de_medida');
            $table->enum('unidad_de_medida', ['Litro', 'Kilo']);

            //..CREA COLUMNAS created_at y updated_at
            $table->timestamps(); ///----NOTA:VARIBLE NECESARIA PARA SAVE()
            //------------------------------------------------------------

            //-------CLAVES/INDICES DE TABLA -----------------------------
            //-- Clave Primaria
            //$table->primary('materia_prima_id');

            //-- clave foranea
            $table->foreign('persona_id')
                ->references('persona_id')
                ->on('usuario');
                //->onUpdate('cascade');


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //materia_prima
        Schema::drop('materia_prima');
    }
}
