<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoLacteoTable extends Migration
{


    public function up()
    {
        //
        Schema::create('producto_lacteo', function (Blueprint $table) {

            //-------VARIABLES/COLUMNAS DE TABLA -------------------------
            $table->integer('persona_id')->unsigned(); //--->CF--hacia usuario
            $table->increments('producto_id');//--increments ==>CP x defecto
            $table->string('nombre_producto_lacteo');
            $table->string('descripcion');
            //$table->string('tipo');
            $table->enum('tipo', ['Bebida', 'Dulce', 'Helado', 'Queso']);
            $table->integer('dias_tiempo_de_vencimiento')->unsigned();//---dias
            $table->string('instrucciones')->nullable();
            //$table->string('unidad_de_medida');
            $table->enum('unidad_de_medida', ['Litro', 'Kilo']);

            //..CREA COLUMNAS created_at y updated_at
            $table->timestamps(); ///----NOTA:VARIBLE NECESARIA PARA SAVE()
            //------------------------------------------------------------

            //-------CLAVES/INDICES DE TABLA -----------------------------
            //-- Clave Primaria
            //$table->primary('producto_id');

            //-- clave foranea
            $table->foreign('persona_id')
                ->references('persona_id')
                ->on('usuario');
                //->onUpdate('cascade');


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('producto_lacteo');
    }
}
