<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //===>>tabla interseccion entre (producto_lacteo X materia_prima)
        Schema::create('ingrediente', function (Blueprint $table) {

            //-------VARIABLES/COLUMNAS DE TABLA -------------------------
            $table->integer('materia_prima_id')->unsigned(); //--CF (materia_prima)
            $table->integer('producto_id')->unsigned();     //--CF (producto_lacteo)

            $table->integer('orden')->unsigned();

            //..CREA COLUMNAS created_at y updated_at
            $table->timestamps(); ///----NOTA:VARIBLE NECESARIA PARA SAVE()
            //------------------------------------------------------------

            //-------CLAVES/INDICES DE TABLA -----------------------------

            //-- clave primaria COMPUESTA
            $table->primary(['materia_prima_id', 'producto_id']);//CP compuesta

            //-- clave foranea
            //---CF materia_prima
            $table->foreign('materia_prima_id')
                ->references('materia_prima_id')
                ->on('materia_prima');
                //->onUpdate('cascade');

            //---CF producto_lacteo
            $table->foreign('producto_id')
                ->references('producto_id')
                ->on('producto_lacteo');
                //->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('ingrediente');
    }
}
