<?php

use Illuminate\Database\Seeder;

class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('persona')->insert([
            'persona_id' => 1,
            'nombre' => 'alejendro',
        ]);

        DB::table('usuario')->insert([
            'persona_id' => 1,
            'nombre_usuario' => 'alejo',
            //'password' => bcrypt('secret'),
            'password' => '123',
        	'rol' => 'Director',
        ]);

    }


}
