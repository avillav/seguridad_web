

@if(Session::has('success'))

<br>
<div class="alert alert-success" style="text-align: center;background-color: #86d266;color: white;">

<button type="button" name="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    {{Session::get("success")}}
    <script type="text/javascript">
      //alert("mensaje");
    </script>


</div>


@elseif(Session::has('error'))

<br>
<div class="alert alert-danger" role="alert" style="text-align: center;background-color: #f34242;color: white;">

<button type="button" name="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    {{Session::get("error")}}
    <script type="text/javascript">
      //alert("mensaje");
    </script>


</div>



@endif
