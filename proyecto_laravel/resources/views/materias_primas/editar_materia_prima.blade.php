@extends('principal')
@section('menu')
<section>
  <div class="container">
    <h3 class="center_text">EDITAR MATERIA PRIMA </h3>
  </div>
  <br>
  <div class="container center_form">
    <form class="" action="editar_materia_prima" method="post">
      <!-- token de form-->
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

      <div class="form-group">
        <label for="nombre">Nombre</label>
        <input class="form-control"type="text" name="nombre_materia_prima" value="{{$mi_materiaPrima->nombre_materia_prima}}">
        <!--hidden con id -->
        <input type="hidden" name="materia_prima_id" value="{{$mi_materiaPrima->materia_prima_id}}">
      </div>

      <div class="form-group">
        <label for="">Descripción</label>
        <textarea class="form-control"name="descripcion" rows="8" cols="80">{{$mi_materiaPrima->descripcion}}</textarea>
      </div>

      <div class="form-group">
        <label for="option">Tipo</label>
        <select class="form-control" name="tipo">
          <option value="{{$mi_materiaPrima->tipo}}">{{$mi_materiaPrima->tipo}}</option>
          <option value="Comestible">Comestible</option>
          <option value="Empaque">Empaque</option>
        </select>
      </div>

      <div class="form-group">
        <label for="option">Unidad</label>
        <select class="form-control" name="unidad_de_medida">
          <option value="{{$mi_materiaPrima->unidad_de_medida}}">{{$mi_materiaPrima->unidad_de_medida}}</option>
          <option value="Litro">Litro</option>
          <option value="Kilo">Kilo</option>
        </select>
      </div>

      <div class="form-group">
        <label for="option">Producto</label>
        <select class="form-control" name="">
          <option value="">QUESO</option>
          <option value="">2</option>
        </select>
      </div>

      <input type="submit" name="btn_editar" value="Editar Materia prima" class="btn btn-warning">
    </form>
  </div>
</section>
@endsection
