@extends('principal')



@section('menu')

@include('alerts.mensajes')
<style>
    @import url(css/mi_estilo.css);
</style>


<section>






  <div class="container">
    <h3 class="center_text">LISTAR MATERIA PRIMA </h3>
  </div>
  <br>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <table class="table">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Tipo</th>
              <th>Unidades</th>
              <th>Eliminar</th>
              <th>Editar</th>
            </tr>
          </thead>
          <tbody>

            <!---  < ?php print_r($materiasPrimas) ?> -->
            <!--- inicio ciclo para pintar materias -->
            @foreach($materiasPrimas as $materiaPrima)

            <tr>
              <td>{{$materiaPrima->nombre_materia_prima}}</td>
              <td>{{$materiaPrima->tipo}}</td>
              <td>{{$materiaPrima->unidad_de_medida}}</td>
              <td><a class="btn btn-danger"  href="eliminar_materia_prima?materia_prima_id={{$materiaPrima->materia_prima_id}}" >Eliminar</a> </td>
              <td><a class="btn btn-warning" href="editar_materia_prima?materia_prima_id={{$materiaPrima->materia_prima_id}}" >Editar</a> </td>
            </tr>

            <!--- fin ciclo materias -->
            @endforeach
          </tbody>
        </table>

      </div>
      <div class="cell">
        <a href="/registro_materia_prima">
         <button class="btn btn-primary "type="button" name="button" value="registrar" data-reactid=".0.0.7" >Registrar</button></a>

     </div>

    </div>

  </div>
</section>

@endsection
