@extends('principal')
@section('menu')
<section>
  <div class="container">
    <h3 class="center_text">REGISTRAR MATERIA PRIMA </h3>
  </div>
  <br>
  <div class="container center_form">
    <form class="" action="registro_materia_prima" method="post">
      <!-- token de form-->
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

      <div class="form-group">
        <label for="nombre">Nombre</label>
        <input class="form-control"type="text" name="nombre_materia_prima" value="">
      </div>
      <div class="form-group">
        <label for="">Descripción</label>
        <textarea class="form-control"name="descripcion" rows="8" cols="80"></textarea>
      </div>
      <div class="form-group">
        <label for="option">Tipo</label>
        <select class="form-control" name="tipo">
          <option value="Comestible">Comestible</option>
          <option value="Empaque">Empaque</option>

        </select>
      </div>
      <div class="form-group">
        <label for="option">Unidad</label>
        <select class="form-control" name="unidad_de_medida">
          <option value="Litro">Litro</option>
          <option value="Kilo">Kilo</option>
        </select>
      </div>
      <div class="form-group">
        <label for="option">Producto</label>
        <select class="form-control" name="">
          <option value="">QUESO</option>
          <option value="">2</option>

        </select>
      </div>

      <input type="submit" name="btn_registrar" value="Registrar Materia prima" class="btn btn-primary">
    </form>
  </div>
</section>
@endsection
