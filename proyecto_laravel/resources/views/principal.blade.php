<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Home</title>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap -->
     <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">


     <!-- jQuery -->
     <script src="js/jquery-1.9.1.js"></script>




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

    <body>
        <!-- Cabecera -->
        <header class="fondo_header">
          <div class="logo">
            <img class="img-circle"src="img/milk.png" alt="">
          </div>
          <div class="titular">
            <h1 class="titulo">Laboratorio de lacteos</h1>

          </div>
          <!-- <div class=" panel panel-default">
            <div class="panel-body">
                <div class="col-xs-6">
                  <ul class="nav nav-tabs ">
                    <li role="presentation"class=" active " ><a href="#">Inicio</a></li>
                    <li role="presentation"><a href="registro_usuario">Usuario</a></li>
                    <li role="presentation"><a href="registro_materia_prima">Mataria prima</a></li>
                    <li role="presentation"><a href="producto_lacteo">Producto lacteo</a></li>
                  </ul>
                </div>
              </div>
          </div> -->


        </header>



        <section>
          <nav>
            <ul class="menu">
              <li><a href="home"><h4>Inicio</h4></a></li>
              <li><a href="editar_usuario"><h4>mi Usuario</h4></a></li>
              <li><a href="materia_prima"><h4>mis Matarias primas</h4></a></li>
              <li><a href="producto_derivado"><h4>mis Productos lacteos</h4></a></li>

              <!-- si es DIRECTOR ==> agregar campo de ADMIN -->
              @if (session('usuario.rol') == 'Director')
                    <li><a href="home_administrador"><h4>ADMIN</h4></a></li>
              @endif


              <li class="btn_logout"><a href="/logout"><input type="button" class="btn btn-primary"  name="btn_logout_user" value="Logout"></a></li>
            </ul>

          </nav>
        </section>
        <!--CONTENIDO-->






      @yield('menu')

        <!-- @yield('contenido') -->

  <!---FIN DE CONTENIDO-->
  <footer class="center_text center-on-small-only pt-0" id="myFooter" >

    <div class="container footer-style">
    <!-- <h3>Multi-Coloured</h3> -->
        <hr>
            <div class="text-center center-block">
                <p class="txt-railway"> Contacto </p>
                <br />
                  <a href="https://www.facebook.com/bootsnipp">
                    <i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
    	            <a href="https://twitter.com/bootsnipp">
                    <i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
    	            <a href="https://plus.google.com/+Bootsnipp-page">
                    <i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
    	            <a href="mailto:bootsnipp@gmail.com">
                    <i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
            </div>
        <hr>
    </div>

      <!--Copyright-->

      <div class="container footer-copyright">
          <div class="container-fluid ">
              © 2016 Copyleft: <a href="#"> Alejo villa - Miller Ossa S </a>
          </div>
      </div>
      <!--/Copyright-->

  </footer>
  <!--/Footer-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <style>
            @import url(css/mi_estilo.css);
        </style>
    </body>
</html>
