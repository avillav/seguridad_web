@extends('principal')
@section('menu')


<section>
  <div class="container">
    <h3 class="center_text">EDITAR PRODUCTO </h3>
  </div>
  <br>
  <div class="container center_form">
    <form class="" action="editar_producto" method="post">
      <!-- token de form-->
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

      <div class="form-group">
        <label for="nombre">Nombre</label>
        <input class="form-control"type="text" name="nombre_producto_lacteo" value="{{$mi_producto->nombre_producto_lacteo}}">
        <!--hidden con id -->
        <input type="hidden" name="producto_id" value="{{$mi_producto->producto_id}}">
      </div>

      <div class="form-group">
        <label for="">Descripción</label>
        <textarea class="form-control"name="descripcion" rows="8" cols="80">{{$mi_producto->descripcion}}</textarea>
      </div>

      <div class="form-group">
        <label for="option">Tipo</label>
        <select class="form-control" name="tipo">
          <option value="{{$mi_producto->tipo}}">{{$mi_producto->tipo}}</option>
          <option value="Queso">Queso</option>
          <option value="Helado">Helado</option>
          <option value="Dulce">Dulce</option>
          <option value="Bebida">Bebida</option>
        </select>
      </div>

      <div class="form-group">
        <label for="">dias de vencimiento</label>
        <input class="form-control" type="number" name="dias_tiempo_de_vencimiento" value="{{$mi_producto->dias_tiempo_de_vencimiento}}">
      </div>

      <div class="form-group">
        <label for="">instrucciones</label>
        <input class="form-control"type="text" value="{{$mi_producto->instrucciones}}" name="instrucciones" >
      </div>

      <div class="form-group">
        <label for="option">Unidad</label>
        <select class="form-control" name="unidad_de_medida">
          <option value="{{$mi_producto->unidad_de_medida}}">{{$mi_producto->unidad_de_medida}}</option>
          <option value="Litro">Litro</option>
          <option value="Kilo">Kilo</option>
        </select>
      </div>

      <div class="form-group">
        <label for="option">Materia prima</label>
        <select class="form-control" name="">
          <option value="">Leche</option>
          <option value="">2</option>
          <option value="">3</option>
          <option value="">4</option>
        </select>
      </div>

      <input type="submit" name="btn_editar" value="Editar Producto" class="btn btn-warning">
    </form>
  </div>
</section>
@endsection
