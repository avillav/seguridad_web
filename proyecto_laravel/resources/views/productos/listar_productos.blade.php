@extends('principal')
@section('menu')
<style>
    @import url(css/mi_estilo.css);
</style>

<section>
  <div class="container">
    <h3 class="center_text">LISTAR PRODUCTO LACTEO </h3>
  </div>
  <br>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <table class="table">
          <thead>
            <tr>
              <th>PRODUCTO</th>
              <th class="centrar_texto">Tipo</th>
              <th class="centrar_texto">Dias Vence</th>
              <th class="centrar_texto">Unidades</th>
              <th class="centrar_texto">Eliminar</th>
              <th class="centrar_texto">Editar</th>
              <th class="centrar_texto">MATERIAS PRIMAS</th>
            </tr>
          </thead>
          <tbody>

            @foreach($productosLacteos as $productoLacteo)
            <tr>
              <th>{{$productoLacteo->nombre_producto_lacteo}}</th>
              <td class="centrar_texto">{{$productoLacteo->tipo}}</td>
              <td class="centrar_texto">{{$productoLacteo->dias_tiempo_de_vencimiento}}</td>
              <td class="centrar_texto">{{$productoLacteo->unidad_de_medida}}</td>
              <td class="centrar_texto"><a class="btn btn-danger"  href="eliminar_producto?producto_id={{$productoLacteo->producto_id}}" >Eliminar</a> </td>
              <td class="centrar_texto"><a class="btn btn-warning" href="editar_producto?producto_id={{$productoLacteo->producto_id}}" >Editar</a> </td>

                <td>
                  <table class="table" style="border-style: double;">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Orden</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Nombre1</td>
                        <td style="text-align: center;">1</td>
                      </tr>

                      <tr>
                        <td>Nombre2</td>
                        <td style="text-align: center;">2</td>
                      </tr>

                    </tbody>
                  </table>

                </td>

            </tr>

            @endforeach
          </tbody>
        </table>


           <!-- NO PONER BUTON SI envian parametro == persona_id -->
           @if (!request('persona_id'))
                  <div class="cell">
                    <a href="/registro_producto"><button class="btn btn-primary "type="button" name="button" value="registrar" data-reactid=".0.0.7" >Registrar</button></a>
                  </div>
           @endif




      </div>

    </div>

  </div>
</section>

@endsection
