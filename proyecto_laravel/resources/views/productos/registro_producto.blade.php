@extends('principal')
@section('menu')


<section>
  <div class="container">
    <h3 class="center_text">REGISTRAR PRODUCTO </h3>
  </div>
  <br>
  <div class="container center_form">
    <form class="" action="registro_producto" method="post">
      <!-- token de form-->
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

      <div class="form-group">
        <label for="nombre">Nombre</label>
        <input class="form-control"type="text" name="nombre_producto_lacteo" value="">
      </div>

      <div class="form-group">
        <label for="">Descripción</label>
        <textarea class="form-control"name="descripcion" rows="8" cols="80"></textarea>
      </div>

      <div class="form-group">
        <label for="option">Tipo</label>
        <select class="form-control" name="tipo">
          <option value="Queso">Queso</option>
          <option value="Helado">Helado</option>
          <option value="Dulce">Dulce</option>
          <option value="Bebida">Bebida</option>
        </select>
      </div>

      <div class="form-group">
        <label for="">dias de vencimiento</label>
        <input class="form-control" type="number" name="dias_tiempo_de_vencimiento" value="">
      </div>

      <div class="form-group">
        <label for="">instrucciones</label>
        <input class="form-control"type="text" value="xxxx" name="instrucciones" >
      </div>

      <div class="form-group">
        <label for="option">Unidad</label>
        <select class="form-control" name="unidad_de_medida">
          <option value="Litro">Litro</option>
          <option value="Kilo">Kilo</option>
        </select>
      </div>

      <br><br>

      <div style="border-style: double;padding: 10px;" id="id_raiz_div">


        <div class="form-group">
          <label for="option">Materia prima</label>
          <select class="form-control" name="">
            <option value="">Leche</option>
            <option value="">2</option>
            <option value="">3</option>
            <option value="">4</option>
          </select>
          <input type="number" name="" value="">
        </div>



      </div>
      <button type="button" name="btn_agregar" id="id_btn_agregar">agregar</button>
      <button type="button" name="btn_borrar" id="id_btn_borrar">borrar</button>


      <br><br>
      <input type="submit" name="btn_registrar" value="Registrar Producto" class="btn btn-primary">
    </form>
  </div>
</section>




<script>

    $(document).ready(function(){
      //console.log("hola");

       $("#id_btn_agregar").click(function (e) {

         raiz = document.getElementById('id_raiz_div');  //id de div de campos

           div_contenedor1 = document.createElement('div');
           div_contenedor1.className  = 'form-group';
           raiz.appendChild(div_contenedor1); // agregar a raiz


              //crear label
              txt_materia = document.createElement('label');
              txt_materia.innerText  = 'Materia prima';
              div_contenedor1.appendChild(txt_materia);

              //crear select
              select_materia = document.createElement('select');
              //select_dia_desde.name = 'dia_desde'+icremento_horario;
              select_materia.className  = 'form-control';
              div_contenedor1.appendChild(select_materia);

                option_materia = document.createElement('option');
                option_materia.innerText = 'Azucar';
                option_materia.value = 'Azucar';
                select_materia.appendChild(option_materia);

                option_materia2 = document.createElement('option');
                option_materia2.innerText = 'Leche';
                option_materia2.value = 'Leche';
                select_materia.appendChild(option_materia2);

              //crear label
              txt_orden = document.createElement('input');
              txt_orden.type = 'number';
              div_contenedor1.appendChild(txt_orden);


         //alert("click boton");


       });

      //-->>declarar variables
      var vector_materias = [];
      var item_materia    = {};
      //console.log(vector_materias);
      //console.log(item_materia);
      //alert("variables");

      //-->>llenar vector_materias[] con items{} de materia


      //-->>enviar datos
      //aInfo = JSON.stringify(vector_materias);
      aInfo = 'pepe';
      //alert(aInfo);
      var identificacion = aInfo;
      var cadenaFormulario = "&data=" + identificacion;

      //alert(cadenaFormulario);

      $.ajax({
      data: cadenaFormulario,
              dataType: "html",
              type: 'GET',
              url : 'registro_producto2',
              processData: false,
              contentType: false,
              success: function(r){

                //console.log("ajax_datos");
                //alert("ajax_datos");
                //console.log(r);

                //return r;
              }
      });

    });

</script>

@endsection
