@extends('principal')
@section('menu')


<section>
  <div class="container">
    <h3 class="center_text">EDITAR PERFIL DE USUARIO </h3>
  </div>
  <br>
  <div class="container center_form">
    <form class="" action="registro_usuario" method="post">
      <!-- token de form post-->
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">



      <div class="form-group">
        <label for="option">Nombre</label>
        <input class="form-control" type="text" name="nombre" value="{{$mi_usuario['nombre']}}">
      </div>

      <div class="form-group">
        <label for="nombre">Username</label>
        <input class="form-control"type="text" name="nombre_usuario" value="{{$mi_usuario['nombre_usuario']}}">
      </div>

      <div class="form-group">
        <label for="form-control">password</label>
        <input class="form-control"type="password" name="password" value="{{$mi_usuario['password']}}">
      </div>

      <div class="form-group">
        <label for="option">Rol</label>
        <select class="form-control" name="rol">
          <option value="">{{$mi_usuario['rol']}}</option>
          <option value="">Estudiante</option>
          <option value="">Tecnico</option>
        </select>
      </div>

      <input type="submit" class="btn btn-primary name="btn_crear_usuario" value="Crear Usuario">
    </form>
  </div>
</section>
@endsection
