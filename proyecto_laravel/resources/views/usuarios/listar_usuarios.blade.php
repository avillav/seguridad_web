@extends('principal')
@section('menu')
<style>
    @import url(css/mi_estilo.css);
</style>

<section>
  <div class="container">
    <h3 class="center_text">USUARIOS </h3>
  </div>
  <br>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <table class="table">
          <thead>
            <tr>
              <th>Nombre de usuario</th>
              <th>Destalles de Productos</th>
            </tr>
          </thead>
          <tbody>

            @foreach($all_usuarios as $one_usuario)
            <tr>
              <td>{{$one_usuario->nombre_usuario}}</td>
              <td><a class="btn btn-primary" href="producto_derivado?persona_id={{$one_usuario->persona_id}}" >Ver Productos</a> </td>

            </tr>

            @endforeach
          </tbody>
        </table>

      </div>

    </div>

  </div>
</section>

@endsection
