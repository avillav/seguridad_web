@extends('principal')
@section('menu')


<section>
  <div class="container">
    <h3 class="center_text">REGISTRAR USUARIO </h3>
  </div>
  <br>
  <div class="container center_form">
    <form class="" action="registro_usuario" method="post">
      <!-- token de form post-->
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

      <div class="form-group">
        <label for="option">Nombre</label>
        <input class="form-control" type="text" name="nombre" >
      </div>

      <div class="form-group">
        <label for="nombre">Username</label>
        <input class="form-control"type="text" name="nombre_usuario">
      </div>

      <div class="form-group">
        <label for="form-control">password</label>
        <input class="form-control"type="password" name="password" >
      </div>

      <div class="form-group">
        <label for="option">Rol</label>
        <select class="form-control" name="rol">
          <option value="Estudiante">Estudiante</option>
          <option value="Tecnico">Tecnico</option>
        </select>
      </div>

      <input type="submit" class="btn btn-primary name="btn_crear_usuario" value="Crear Usuario">
    </form>
  </div>
</section>
@endsection
