
<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Crear Usuario</div>

                    <form  action="vista_crear_usuario" method="post">

                    <!--  IMPORTANTE PONER ESTE TOKEN -->
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <!-- ********* TOKEN ************ --> 
                        
                        <div class="crear_user">

                            <input type="text" placeholder="iduser" name="user"><br>
                            <input type="text" placeholder="idpass" name="pass"><br>
                            <!-- <input type="button" value="Login"> -->
                            <input type="submit" name="btn_crear" value="crear usuario">
                        </div>
                    </form>

            </div>
        </div>

       



    </body>
</html>
